# @voerkalogger/storage

## 1.1.13

### Patch Changes

- 90a7fbd: fix deps
- 2b5fdf6: add storage
- Updated dependencies [90a7fbd]
  - @voerkalogger/core@1.1.13
